#include <SoftSerial.h>     /* Allows Pin Change Interrupt Vector Sharing */

int TX = 3;
int RX = 4;

SoftSerial mySerial(RX, TX); // RX, TX

// Analog input pin
// To set to input: pinMode(2, INPUT);
int input = 1; //P2 PCB

// Pin of the led to over voltage
int led = 1; //P1 PCB

void setup() {
  // set the data rate for the SoftwareSerial port
  mySerial.begin(115200);

  pinMode(led, OUTPUT);
}

void loop() {

  int value = 0;
  float avg = 0.0;

  for (int i = 0; i < 1000; i++) {
    value = analogRead(input);
    avg += (float)value;

    delay(1);
  }
  
  int data = avg/1000;

  // Led to info of over voltage
  if (data > 1000) {
    digitalWrite(led, HIGH);
  } else {
    digitalWrite(led, LOW);
  }
  
  mySerial.print(data);
}

