#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>

ESP8266WiFiMulti wifiMulti;

unsigned long previousMillis = 0;

const int   infoLed = 2;

const char* ssid     = "NigmA";
const char* password = "nigma1234";

const char* host  = "192.168.137.1";
const int   port  = 8080;
const String resource = "esp/ESPServlet";

float voltage = 0.0;

int request_time = 10;   //Seconds

void setup() {
  Serial.begin(115200);
  delay(1000);

  //Setup the led
  pinMode(infoLed, OUTPUT);
  digitalWrite(infoLed, HIGH);

  // Setup the wifi networks
  WiFi.mode(WIFI_STA);
  wifiMulti.addAP(ssid, password);

  // Conect to a network
  Serial.println("Connecting Wifi...");
  while (wifiMulti.run() != WL_CONNECTED) {
    Serial.print(".");
    delay(500);
  }

  // WiFi connected
  digitalWrite(infoLed, LOW);

  Serial.println("\nWiFi connected");
  Serial.print("SSID:\t");
  Serial.println(WiFi.SSID());
  Serial.print("IP:\t");
  Serial.println(WiFi.localIP());
}

void loop() {
  getVoltage();

  if (wifiMulti.run() == WL_CONNECTED) {
    
    unsigned long currentMillis = millis();
    
    if ( currentMillis - previousMillis > (request_time * 1000) ) {
      previousMillis = currentMillis;
      
      sendData("dato=" + String(voltage,2));
               
    }
  } else {
    digitalWrite(infoLed, HIGH);

    // Conect to a network
    Serial.println("\nConnection lost, reconnecting Wifi...");
    while (wifiMulti.run() != WL_CONNECTED) {
      Serial.print(".");
      delay(500);
    }

    // WiFi connected
    digitalWrite(infoLed, LOW);

    Serial.println("\nWiFi connected");
    Serial.print("SSID:\t");
    Serial.println(WiFi.SSID());
    Serial.print("IP:\t");
    Serial.println(WiFi.localIP());
  }
}

void sendData(String data) {
  WiFiClient client;

  if (client.connect(host, port)) {
    // POST request
    client.print(String("POST /") + resource + " HTTP/1.1\r\n" +
                 "Host: " + String(host) + ":" + String(port) + "\r\n" +
                 "Content-Length: " + data.length() + "\r\n" +
                 "Content-Type:" + "application/x-www-form-urlencoded\r\n" +
                 "\r\n" +
                 data);

    // Print the response
    if (client.available()) {
      String line = client.readStringUntil('\n');
      Serial.println(line);
    }
  } else {

    // Connection with the host fail
    for (int i = 0; i < 4; i++) {
      digitalWrite(infoLed, HIGH);
      delay(50);
      digitalWrite(infoLed, LOW);
      delay(50);
    }
  }
}

void getVoltage() {
  // Voltage divider max 7.4v
  // Resistor value
  float R1 = 1000.0;  // Positive termonal
  float R2 = 1000.0;  // Negative terminal

  if (Serial.available() > 0) {

    String data = Serial.readString();

    voltage = ((R1 + R2) / R2) * data.toFloat() * (4.64 / 1023.0);
  }
}

